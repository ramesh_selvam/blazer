import 'package:flutter/material.dart';

import '../../Network.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Blazers"),
      ),
      body: Container(
        child: FutureBuilder(
            future: Database().getUsersData(),
            builder: (context, snapshot) {
              if (snapshot.hasData)
                return ListView.builder(
                    itemCount: snapshot.data.length,
                    itemBuilder: (context, index) {
                      return Container(
                        child: ListTile(
                            contentPadding: EdgeInsets.symmetric(
                                horizontal: 20.0, vertical: 10.0),
                            leading: Container(
                              padding: EdgeInsets.only(right: 12.0),
                              decoration: new BoxDecoration(
                                  border: new Border(
                                      right: new BorderSide(
                                          width: 1.0, color: Colors.white24))),
                              child: Icon(Icons.autorenew, color: Colors.white),
                            ),
                            title: Text(
                             snapshot.data[index]["displayName"],
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                            // subtitle: Text("Intermediate", style: TextStyle(color: Colors.white)),

                            subtitle: Row(
                              children: <Widget>[
                                Icon(Icons.offline_pin,
                                    color: (snapshot.data[index]["isAvailable"] == "true") ? Colors.greenAccent : Colors.redAccent),
                                Text(snapshot.data[index]["email"],
                                    style: TextStyle(color: Colors.white))
                              ],
                            ),
                            trailing: Row(
                              children: <Widget>[
//                                ToggleButtons(isSelected: , children: <Widget>[Text("ON")],),
                                Icon(Icons.keyboard_arrow_right,
                                    color: Colors.white, size: 30.0),
                              ],
                            )),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                            boxShadow: <BoxShadow>[
                              BoxShadow(
                                  color: Colors.grey.shade200,
                                  offset: Offset(2, 4),
                                  blurRadius: 5,
                                  spreadRadius: 2)
                            ],
                            gradient: LinearGradient(
                                begin: Alignment.centerLeft,
                                end: Alignment.centerRight,
                                colors: [
                                  Color(0xfffbb448),
                                  Color(0xfff7892b)
                                ])),
                      );
                    });
              else
                return Container();
            }),
      ),
    );
  }
}
